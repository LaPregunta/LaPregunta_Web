
<!DOCTYPE html>
<html lang="pt-BR">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../site1/brunao.css">
    <link rel="shortcut icon" href="../assets/img/favicon.ico">
    <script src="https://use.fontawesome.com/8ed945b9d4.js"></script>
    <title>LaPregunta?</title>

    <style type="text/css">
        .corTexto {
         color:white;
        }
    </style>


</head>

<body class="body">

<!-- Barra de navegação -->

<nav class="navbar bg-faded navbar-dark font"  >

    <a href="../site1/index.php" class="navbar-brand">
        <img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
        <!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
    </a>

    <!--botão sanduíche -->
    <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

    <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
    <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

        <ul class="nav navbar-nav">

            <li class="nav-item">
                <a href="../site1/index.php" class="nav-link">Home</a>
            </li>

            <li class="nav-item">
                <a href="../site1/sobre.php" class="nav-link">Sobre o Aplicativo</a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                <div class="dropdown-menu">
                    <a href="android.php" class="dropdown-item"><i class="fa fa-android" aria-hidden="true"></i>  Android</a>
                    <a href="desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
                </div>
            </li>
            <li class="nav-item">
            <a href="login.php" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a>
            </li>



        </ul>


        <!-- pesquisa -->
        <form class="form-inline" style="text-align: right;">
            <input type="text" class="form-control" placeholder="digite a pesquisa...">
            <button class="btn btn-outline-secondary  btn-dm" type="submit">Ok</button>
        </form>

    </div> <!-- sanduíche -->

</nav>




<!-- Fim Barra de navegação -->
<div class="jumbotron jumbotron-fluid" text-align="center"  style="background-color: #D8D8FF">
    <div class="container">

        <div class="jumbotron" style="background-color: #190033" >

            <?php
        require_once "../conexao/conexao.php";

        $acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
        $id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
        $senha = isset($_POST['senha'])?$_POST['senha']:null;
        $email = isset($_POST['email'])?$_POST['email']:null;

        $mensagem = array();

        ?>

        <h1 class="corTexto" class="display-3" style="font-variant: small-caps" > Cadastro LaPregunta?</h1>
        <br>
            <?php


            if($acao=="incluir") {

                $erros = false;
                if (isset($_REQUEST['nome']) && ($_REQUEST['nome'] != '')) {
                    $nome = $_REQUEST['nome'];
                } else {
                    echo "Campo NOME não pode ser vazio!<br>";
                    $erros = true;
                    exit();
                }


                if (isset($_REQUEST['email']) && ($_REQUEST['email'] != '')) {
                    $email = $_REQUEST['email'];
                } else {
                    echo "Campo EMAIL não pode ser vazio!<br>";
                    $erros = true;
                    exit();
                }

                if (isset($_REQUEST['senha']) && ($_REQUEST['senha'] != '')) {
                    $senha = $_REQUEST['senha'];
                } else {
                    echo "Campo SENHA não pode ser vazio!<br>";
                    $erros = true;
                    exit();
                }

                if (isset($_REQUEST['email']) && ($_REQUEST['email'] != '')) {
                    $email = $_REQUEST['email'];
                } else {
                    echo "Já existe um usuário com este email!<br>";
                    $erros = true;
                    exit();
                }


                if (!$erros) {
                    $sql = "INSERT INTO AlunoLP  ";
                    $sql .= "( nome, email, senha)";
                    $sql .= " VALUES (?,?,?) ";
                    $stmt = $cn->prepare($sql);
                    $stmt->bindParam(1, $nome);
                    $stmt->bindParam(2, $email);
                    $stmt->bindParam(3, $senha);
                    if ($stmt->execute()) {
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                        $headers .= 'From: LaPregunta <denadaijulia@gmail.com>';
                        $mensagemEmail = 'Olá, este é um email enviado pelo ProjetoDinfoApp.';
                        $enviou = mail($email,'Email de verificação',$mensagemEmail);
                        if($enviou){

                        }

                        $erro = "Usuário cadastrado com sucesso!";
                        echo $erro;
                        print "<meta http-equiv='refresh' content='1;url=login.php'>";
                        exit();

                    } else {
                        $erro = $stmt->errorCode();
                        $mensagem[$erro] = implode(",", $stmt->errorInfo());
                    }

                }
            }
            ?><?php
            if(count($mensagem)>0){
                foreach ($mensagem as $msg){
                    echo $msg;
                }
            }
            ?>
        <form action="?acao=incluir" method="post">

                  <div class="form-group">
                    <label for="txtNome" class="corTexto">Nome:</label>
                    <input type="text" class="form-control" name="nome" placeholder="Insira seu nome">
                  </div>

                    <div class="form-group">
                        <label for="txtNome" class="corTexto">Email:</label>
                        <input type="text" class="form-control" name="email" placeholder="Insira seu email">
                    </div>

                  <div class="form-group">
                    <label for="txtSenha" class="corTexto">Senha:</label>
                    <input type="password" class="form-control" name="senha" placeholder="Insira sua senha">
                  </div>


                  <div class="form-group">
                    <label for="sltPerfil" class="corTexto">Perfil:</label>
s
                    <select id="sltPerfil" class="form-control">
                      <option>Aluno</option>
                      <option>Professor</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="sltCidade" class="corTexto">Cidade:</label>
                    
                    <select multiple="" id="sltCidade" class="form-control">
                      <option>São Paulo</option>
                      <option>Rio de Janeiro</option>
                      <option>Curitiba</option>
                    </select>
                  </div>                  




            <br>
                    <input type="submit" name="btnEnviar" class="btn btn-outline-secondary  btn-lg" class="corTexto">
                </form>


                </div>
        <hr class="my-5">





        
        <br>
        <br>
        <br>


        

</div>

<script src="../assets/js/jquery-3.1.0.js"></script>
<script src="../assets/js/bootstrap.js"></script>


</body>

</html>
