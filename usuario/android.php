<?php
error_reporting(E_ALL);
session_start();
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../site1/brunao.css">

    <link rel="shortcut icon" href="../assets/img/favicon.ico">
    <title>LaPregunta?</title>


    <style type="text/css">
        .corTexto {
            color:white;
        }
    </style>

</head>

<body class="body">

<!-- Barra de navegação -->

<nav class="navbar bg-faded navbar-dark font" >

    <a href="../site1/index.php" class="navbar-brand">
        <img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
        <!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
    </a>

    <!--botão sanduíche -->
    <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

    <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
    <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

        <ul class="nav navbar-nav">

            <li class="nav-item">
                <a href="../site1/index.php" class="nav-link">Home</a>
            </li>

            <li class="nav-item">
                <a href="../site1/sobre.php" class="nav-link">Sobre o Aplicativo</a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                <div class="dropdown-menu">
                    <a href="desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
                </div>
                <?php
                if(!isset($_SESSION['usuario'])) {
                    echo '<li class="nav-item"><a href="../usuario/login.php" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                    echo '<li class="nav-item"><a href="../usuario/cadastro.php" class="nav-link">Cadastrar <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                }else{
                    echo '<li class="nav-item"><a href="../usuario/perguntas.php" class="nav-link" >Responder questões</a></li>';
                    echo '<li class="nav-item"><a href="../usuario/logout.php" class="nav-link" >Logout</a></li>';
                }
                ?>

        </ul>


        <!-- pesquisa -->
        <form class="form-inline" style="text-align: right;">
            <input type="text" class="form-control" placeholder="digite a pesquisa...">
            <button class="btn btn-outline-secondary  btn-dm" type="submit">Ok</button>
        </form>

    </div> <!-- sanduíche -->

</nav>

<?php
require_once  "../conexao/conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$senha = isset($_POST['senha'])?$_POST['senha']:null;
$email = isset($_POST['email'])?$_POST['email']:null;


?>


<br><br>
<div class="container" style="background-color: #D8D8FF">
    <div class="jumbotron" style="background-color: #190033" >
        <h1 class="corTexto">Android </h1>
        <br>
        <hr>
        <a href="sorry.apk" >Baixe!</a>
        <br>


    </div>
    <br>


</div>
<hr>


</div>

</div>
<script src="../assets/js/jquery-3.1.0.js"></script>
<script src="../assets/js/bootstrap.js"></script>


</body>

</html>