<?php
error_reporting(E_ALL);
session_start();
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="../site1/brunao.css">

    <link rel="shortcut icon" href="../assets/img/favicon.ico">
    <title>LaPregunta?</title>


    <style type="text/css">
        .corTexto {
         color:white;
        }
    </style>

</head>

<body class="body">

<!-- Barra de navegação -->

<nav class="navbar bg-faded navbar-dark font" >

    <a href="../site1/index.php" class="navbar-brand">
        <img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
        <!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
    </a>

    <!--botão sanduíche -->
    <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

    <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
    <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

        <ul class="nav navbar-nav">

            <li class="nav-item">
                <a href="../site1/index.php" class="nav-link">Home</a>
            </li>

            <li class="nav-item">
                <a href="../site1/sobre.php" class="nav-link">Sobre o Aplicativo</a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                <div class="dropdown-menu">
                    <a href="android.php" class="dropdown-item"><i class="fa fa-android" aria-hidden="true"></i>  Android</a>
                    <a href="desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
                </div>


            <li class="nav-item">
            <a href="cadastro.php" class="nav-link">Cadastrar <i class="fa fa-sign-in" aria-hidden="true"></i></a>
            </li>

        </ul>


        <!-- pesquisa -->
        <form class="form-inline" style="text-align: right;">
            <input type="text" class="form-control" placeholder="digite a pesquisa...">
            <button class="btn btn-outline-secondary  btn-dm" type="submit">Ok</button>
        </form>

    </div> <!-- sanduíche -->

</nav>

<?php
require_once  "../conexao/conexao.php";

$acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
$senha = isset($_POST['senha'])?$_POST['senha']:null;
$email = isset($_POST['email'])?$_POST['email']:null;


?>

<br><br>
<div class="container" style="background-color: #D8D8FF">
    <div class="jumbotron" style="background-color: #190033" >
        <h1 class="corTexto">Login </h1>
        <br>
        <hr>

        <?php

        if($acao=="autenticar"){
            $sql =("SELECT * FROM AlunoLP WHERE email= '$email' AND senha = '$senha'");
            $stmt = $cn->prepare($sql);

            $stmt->bindParam(':email', $email);
            $stmt->bindParam(':senha', $senha);

            $stmt->execute();

            $users = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if(count($users) > 0){

                $_SESSION['usuario']= 'Usuário';
                $_SESSION['email']=$_POST['email'];
                $_SESSION['senha']=$_POST['senha'];
                $erro = "Usuário logado com sucesso!";
                echo $erro;

                print "<meta http-equiv='refresh' content='1;url=perguntas.php'>";
                exit();
            }else{
                echo "Usuário ou senha inválidos";
                die();
            }
        }

        ?>

        <form action="?acao=autenticar" method="post" >
        <div class="form-group">
            <label for txtemail class="corTexto" >Digite seu email:</label>
            <input type="email" size="50" name="email" id="email" class="form-control">
            
        </div>
            <br>
            <p class="corTexto">Digite sua senha:</p>
            <input type="password" size="10" name="senha" id="senha" class="form-control" >
            <br>
            <br>
            <input type="submit" name="btnEnviar" class="btn btn-outline-secondary  btn-lg" class="corTexto">
            
        
            <br>
            <br>
            <?php
            if(!isset($_SESSION['usuario'])){
                echo '<a href="cadastro.php" class="btn btn-outline-secondary  btn-lg">Ainda não sou cadastrado</a>';
            }
            ?>

        </form>
        <br>


    </div>
        <br>


    </div>
    <hr>


</div>

</div>
<script src="../assets/js/jquery-3.1.0.js"></script>
<script src="../assets/js/bootstrap.js"></script>


</body>

</html>