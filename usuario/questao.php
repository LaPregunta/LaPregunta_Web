<?php
error_reporting(E_ALL);
session_start();
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="../site1/brunao.css">

    <link rel="shortcut icon" href="../assets/img/favicon.ico">
    <title>LaPregunta?</title>


    <style type="text/css">
        .corTexto {
            color:white;
        }
    </style>

</head>

<body class="body">

<!-- Barra de navegação -->

<nav class=" navbar bg-faded navbar-dark font "  >

    <a href="index.php" class="navbar-brand">
        <img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
        <!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
    </a>

    <!--botão sanduíche -->
    <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

    <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
    <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

        <ul class="nav navbar-nav">

            <li class="nav-item">
                <a href="../site1/index.php" class="nav-link">Home</a>
            </li>

            <li class="nav-item">
                <a href="../site1/sobre.php" class="nav-link">Sobre o Aplicativo</a>
            </li>
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                <div class="dropdown-menu">
                    <a href="android.php" class="dropdown-item"><i class="fa fa-android" aria-hidden="true"></i>  Android</a>
                    <a href="desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
                </div>

                <?php
                if(!isset($_SESSION['usuario'])) {
                    echo '<li class="nav-item"><a href="../usuario/login.php" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                    echo '<li class="nav-item"><a href="../usuario/cadastro.php" class="nav-link">Cadastrar <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                }else{
                    echo '<li class="nav-item"><a href="../usuario/logout.php" class="nav-link" >Logout</a></li>';
                }
                ?>



        </ul>


        <!-- pesquisa -->
        <form class="form-inline" style="text-align: right;">
            <input type="text" class="form-control" placeholder="digite a pesquisa...">
            <button class="btn btn-outline-secondary  btn-dm" type="submit">Ok</button>
        </form>

    </div> <!-- sanduíche -->

</nav>

<?php
require_once  "../conexao/conexao.php";

if(isset($_GET['ID'])) {


    $sql = ("SELECT ID_Questao,Pergunta,Alternativa1,Alternativa2,Alternativa3,Alternativa4 FROM QuestaoLP WHERE ID_Questionario = ".$_GET['ID']);
    $stmt = $cn->prepare($sql);
    $stmt->execute();


    $questoes = $stmt->fetchAll(PDO::FETCH_ASSOC);
//print_r($questionarios);

    ?>

    <?php

    $acao = isset($_REQUEST['acao'])?$_REQUEST['acao']:null;
    if($acao=="entregar") {
        print_r($_POST);
        $sql = ("SELECT AlternativaCorreta FROM QuestaoLP WHERE ID_Questionario = " . $_GET['ID']);
        $stmt = $cn->prepare($sql);
        $stmt->execute();
        $questaoe = $_POST['formas-tratamento'];
        $questaoc = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($questoes as $questaoc) {
            if(isset($questaoe)){

            }
            if ($questao['ID_Questao'] == $questaoc[$questao]) {
                echo $questaoc;

            }
            echo    $questaoc;
        }
    }
    ?>


    <br><br>
    <div class="container" style="background-color: #D8D8FF">
        <div class="jumbotron" style="background-color: #190033">
            <h1>Questoes</h1>

            <?php
            if ($questoes) {
                foreach ($questoes as $questao) {
                    ?>
            <form action="?ID=<?php echo $_GET['ID'];?>&acao=entregar" method="post">
                        <div class="jumbotron" style="background-color: #D8D8FF">
                            <h3  href="questao.php"><?php echo $questao["Pergunta"]; ?></h3>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input " name="formas-tratamento[<?=$questao['ID_Questao'];?>]" value="1" id="alt1">
                                        <?php echo $questao["Alternativa1"]; ?>
                                    </label>
                                </div>

                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="formas-tratamento[<?=$questao['ID_Questao'];?>]"  value="2"  id="alt2">
                                        <?php echo $questao["Alternativa2"]; ?>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="formas-tratamento[<?=$questao['ID_Questao'];?>]"  value="3" id="alt3">
                                        <?php echo $questao["Alternativa3"]; ?>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="formas-tratamento[<?=$questao['ID_Questao'];?>]"  value="4" id="alt4">
                                        <?php echo $questao["Alternativa4"]; ?>
                                    </label>
                                </div>


                            <hr></div>





                    <?php
                }
            }

            ?>
                            <button type="submit" class="btn btn-success">Finalizar</button>
            </form>

        </div>
        <br>


    </div>
    <hr>
    <?php
}else{
    echo 'selecione um questionario';
}
?>

</div>

</div>
<script src="../assets/js/jquery-3.1.0.js"></script>
<script src="../assets/js/bootstrap.js"></script>


</body>

</html>