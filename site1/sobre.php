<?php
//inicializando o serviço de sessao
session_start();
require_once "../conexao/conexao.php";

$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
?>
<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
     <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <link rel="stylesheet" href="brunao.css">
    
    <link rel="shortcut icon" href="../assets/img/favicon.ico">
 	<script src="https://use.fontawesome.com/8ed945b9d4.js"></script>
    <title>LaPregunta?</title>
  <style>
    .panel-heading{
          text-align: left;
      }
 </style>


</head>

<body class="body">

    <!-- Barra de navegação -->

    <nav class="navbar bg-faded navbar-dark font" >

          <a href="index.php" class="navbar-brand">
          	<img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
          	<!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
          </a>

          <!--botão sanduíche -->
          <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

          <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
          <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

            <ul class="nav navbar-nav"> 
 
              <li class="nav-item">
                <a href="index.php" class="nav-link">Home</a>
              </li>

              <li class="nav-item">
                <a href="sobre.php" class="nav-link">Sobre o Aplicativo</a>
              </li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                  <div class="dropdown-menu">
                  <a href="../usuario/android.php" class="dropdown-item"><i class="fa fa-android" aria-hidden="true"></i>  Android</a>
                  <a href="../usuario/desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
          </div>



                  <?php
                  if(!isset($_SESSION['usuario'])) {
                      echo '<li class="nav-item"><a href="../usuario/login.php" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                      echo '<li class="nav-item"><a href="../usuario/cadastro.php" class="nav-link">Cadastrar <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                  }else{
                      echo '<li class="nav-item"><a href="../usuario/perguntas.php" class="nav-link" >Responder questões</a></li>';
                      echo '<li class="nav-item"><a href="../usuario/logout.php" class="nav-link" >Logout</a></li>';
                  }
                  ?>


            </ul>
            <!-- pesquisa -->

            <form class="form-inline" style="text-align: right;">
              <input type="text" class="form-control" placeholder="digite a pesquisa...">
              <button class="btn btn-outline-success" type="submit">Ok</button>
            </form>

          </div> <!-- sanduíche -->

    </nav>
    
    <div class="container-fluid" style="background-color: #D8D8FF">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 clas="panel-title"> A PLATAFORMA</h3>
            </div>
            <div class="panel-body">
                O LaPregunta? é uma plataforma de questionários feita na matéria de Desenvolvimento de Projetos pelo grupo "EraPraFuncionar" do 2CTI do COTIL com supervisão do Professor Willian.
            </div>
        </div>


        <br>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 clas="panel-title"> O SITE</h3>
            </div>
            <div class="panel-body">
                O site foi desenvolvido em paralelo com a matéria de Editoriação Gráfica pelo mesmo grupo com supervisão da Professora Simone.
            </div>
        </div>
        <br>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 clas="panel-title"> O COTIL</h3>
            </div>
            <div class="panel-body">
                O Colégio Técnico de Limeira - COTIL, da Universidade Estadual de Campinas, foi criado pela Lei Estadual no 7.655, de 28 de dezembro de 1962, e autorizado a ser instalado e a entrar em funcionamento pela Resolução C.E.E. no 46/66 e Deliberação C.E.E. no 12/70, Diário Oficial de 29/01/72, página 21. A instalação foi em 24 de abril de 1967, dia em que se comemora seu aniversário. Inicialmente recebeu o nome de Colégio Técnico e Industrial de Limeira, tendo como sua mantenedora a Universidade Estadual de Campinas. Iniciou seu funcionamento nas instalações do Ginásio Estadual Industrial Trajano Camargo de Limeira e passou para as novas instalações no "campus" de Limeira da UNICAMP, tendo sido o prédio inaugurado em 9 de setembro de 1973. O primeiro Diretor foi o Professor Manoel da Silva e o atual é o Professor Paulo Sérgio Saran.

                O COTIL oferece Ensino Médio, preparando os alunos também para o vestibular, com a qualidade UNICAMP. Obteve o primeiro lugar no ENEM (das escolas públicas) nos últimos anos, pois incentiva o raciocínio e a capacidade de aprender. Além disso, tem conquistado medalhas de ouro, prata e bronze nas olimpíadas de Matemática e Física e grande êxito no acesso ao ensino superior.
                Atualmente, forma técnicos em Edificações, Enfermagem, Geodésia e Cartografia, Informática, Mecânica e Qualidade e Produtividade, os quais se inserem no mercado de trabalho com segurança.
            </div>
        </div>



       <br>
        <b><center>LOCALIZAÇÃO:</center></b>
        <br>
        <div class="container formataclasse">
      
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5210.463168976569!2d-47.42159570118008!3d-22.565865622677755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe2108428886c8646!2sCotil+-+Unicamp!5e0!3m2!1spt-BR!2sbr!4v1497454119875" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>

    <br><br><br>
   	<script src="../assets/js/jquery-3.1.0.js"></script>
	<script src="../assets/js/bootstrap.js"></script>


</body>

</html>