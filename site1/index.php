<?php
//inicializando o serviço de sessao
session_start();
require_once "../conexao/conexao.php";

$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;
?>
<!DOCTYPE html>
<html lang="pt-BR">

    
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link rel="stylesheet" href="../assets/css/bootstrap-flex.css">
    <link rel="stylesheet" href="brunao.css">
     	<link rel="shortcut icon" href="../assets/img/favicon.ico">
 	<script src="https://use.fontawesome.com/8ed945b9d4.js"></script>
    <title>LaPregunta?</title>
    
    <style type="text/css">
      .corTexto {
         color:white;
        }

      .texto-centralizado {
          text-align: center;
          margin-top: 1%;
          margin-left: 40%;
      }

      .estilo-container {
          margin-left: -110%;
          width: 320%;
          height: 500%;
      }

      .img-centralizada {
          display: block;
          margin-left: auto;
          margin-right: auto;
          margin-top: 1px;
      }
    </style>


</head>

<body class="body">

    <!-- Barra de navegação -->
    
    <nav class=" navbar bg-faded navbar-dark font "  >

          <a href="index.php" class="navbar-brand">
          	<img src="../assets/img/lapregunta.png" style="width: 75px; height: auto;" class="d-inline-block align-top"> LaPregunta?
          	<!-- d-inline-block = elementos subsequentes não quebram linha, nada por cima dele -->
          </a>

          <!--botão sanduíche -->
          <button class="navbar-toggler hidden-md-up float-sm-right" type="button" data-toggle="collapse" data-target="#menu-sanduiche">=</button>

          <!-- todo o menu que sera escondido/colapsado deverá ficar dentro dessa div -->
          <div class="collapse navbar-toggleable-sm" id="menu-sanduiche">

            <ul class="nav navbar-nav"> 
 
              <li class="nav-item">
                <a href="index.php" class="nav-link">Home</a>
              </li>

              <li class="nav-item">
                <a href="sobre.php" class="nav-link">Sobre o Aplicativo</a>
              </li>
              <li class="nav-item dropdown"> 
                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Downloads</a>
                <div class="dropdown-menu">
                  <a href="../usuario/android.php" class="dropdown-item"><i class="fa fa-android" aria-hidden="true"></i>  Android</a>
                  <a href="../usuario/desktop.php" class="dropdown-item"><i class="fa fa-desktop" aria-hidden="true"></i>  Desktop(Professores)</a>
                </div>

                  <?php
                  if(!isset($_SESSION['usuario'])) {
                      echo '<li class="nav-item"><a href="../usuario/login.php" class="nav-link">Login <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                      echo '<li class="nav-item"><a href="../usuario/cadastro.php" class="nav-link">Cadastrar <i class="fa fa-sign-in" aria-hidden="true"></i></a></li>';
                  }else{
                      echo '<li class="nav-item"><a href="../usuario/perguntas.php" class="nav-link" >Responder questões</a></li>';
                      echo '<li class="nav-item"><a href="../usuario/logout.php" class="nav-link" >Logout</a></li>';
                  }
                  ?>

              

            </ul>	

          
            <!-- pesquisa -->
            <form class="form-inline" style="text-align: right;">
              <input type="text" class="form-control" placeholder="digite a pesquisa...">
              <button class="btn btn-outline-secondary  btn-dm" type="submit">Ok</button>
            </form>
          
          </div> <!-- sanduíche -->        

    </nav>




    <!-- Fim Barra de navegação -->


    <div class="container-fluid">
        <div id="carouselExampleIndicators2" class="carousel slide estilo-container" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators2" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner" role="listbox">

                <div class="carousel-item active">
                    <img class="img-centralizada" src="../assets/img/simbolos-comuns-1.png" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h3>Bem-Vindo ao LaPregunta!</h3>
                        <p>Um site desenvolvido exclusivamente para o 2°CTI</p>
                        <p>.</p>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="img-centralizada" src="../assets/img/tumblr_omxnumi0oC1t4osjeo5_1280.png">
                    <div class="carousel-caption d-none d-md-block">

                        <h3>Questionários e testes para você!</h3>
                        <p>Questionários online sobre assuntos vistos em sala de aula!</p>
                        <p>.</p>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="img-centralizada" src="../assets/img/simbolos-comuns-3.png" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block"  class="img-fluid">
                        <h3>Resultado na hora!</h3>
                        <p>Desenvolvido para melhor conforto dos alunos!</p>
                        <p>.</p>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators2" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselExampleIndicators2" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </div>







            <div class="container">

        		<hr class="my-5">
	


            </div>


        <div class="row container-fluid" style="background-color:#D8D8FF">
            
        <div class="col-md-4 col-sm-12 text-centered">

          <img src="../assets/img/WEB.png" class="img-fluid img-thumbnail">  
          <p class="lead titulos formatatexto" >WEB</p>
          <p style="text-align:center" >Versão Web do "LaPregunta?"</p>

        </div>

        <div class="col-md-4 col-sm-12 text-centered">

          <img src="../assets/img/android.jpg" class="img-fluid img-thumbnail">  
          <p class="lead titulos formatatexto" >Android</p>
          <p style="text-align:center">Versão Android do "LaPregunta?"</p>

        </div>        

        <div class="col-md-4 col-sm-12 text-centered">

          <img src="../assets/img/DESKTOP.jpg" class="img-fluid img-thumbnail">  
          <p class="lead titulos formatatexto" >Desktop</p>
          <p style="text-align:center">Versão Desktop do "LaPregunta?"</p>

        </div>
    </div>
            <div class="jumbotron jumbotron-fluid text-centered body" >
                <center><h3 class="display-3">Depoimentos</h3></center>
                <br><br>
          
                <!-- 1a row do card-->
        <div class="row">
            
          <!-- 1o card -->
          <div class="col-md-4 col-sm-12">
            <div class="card" style="background-color: #190033">
              
              <div class="card-block">
                <h4 class="card-title" style="color:#FFFFFF">José</h4>
                <h6 class="card-subtitle text-muted">3CTI</h6>
              </div>
              
              <div class="card-block">
                <p class="card-text" style="color:#FFFFFF">Bom APP, melhor que o site do Matioli!</p>
                <a href="#oai" data-toggle="collapse" class="text-muted" class="corTexto">ler mais...</a>
              </div>
              <div class="collapse" role="tabpanel" id="oai">
                <div class="card-block">
                  <p align="center" class="corTexto">DinfoAPP é um projeto da matéria Desenvolvimento De Aplicações WEB que se estende desde o mês de junho até o final do ano, os alunos têm que elaborar um site com todas as suas páginas e fazê-lo funcionar.</p>
                </div>
              </div>
            </div>
          </div>

          <!-- 2o card -->
          <div class="col-md-4 col-sm-12">
            <div class="card" style="background-color: #190033">
              
              <div class="card-block" >
                <h4 class="card-title" style="color:#FFFFFF">Cuca</h4>
                <h6 class="card-subtitle text-muted">5°MEN</h6>
              </div>
              
              <div class="card-block">
                <p class="card-text" style="color:#FFFFFF">Muito bom, Matioli que se cuide!</p>
              <a href="#oa" data-toggle="collapse" class="text-muted" class="corTexto">ler mais...</a>
              </div>
              <div class="collapse" role="tabpanel" id="oa">
                <div class="card-block">
                  <p align="center" class="corTexto">DinfoAPP é um projeto da matéria Desenvolvimento De Aplicações WEB que se estende desde o mês de junho até o final do ano, os alunos têm que elaborar um site com todas as suas páginas e fazê-lo funcionar.</p>
                </div>
              </div>
            </div>
          </div>

          <!-- 3o card -->
          <div class="col-md-4 col-sm-12">
            <div class="card" style="background-color: #190033">
              
              <div class="card-block"  >
                <h4 class="card-title" style="color:#FFFFFF" >Thomas</h4>
                <h6 class="card-subtitle text-muted">4°INFD</h6>
              </div>
              
              <div class="card-block">
                <p class="card-text" style="color:#FFFFFF">Bom, recomendo a todos os alunos do COTILSOM!!</p>
              <a href="#oaie" data-toggle="collapse" class="text-muted" class="corTexto">ler mais...</a>
              </div>
              <div class="collapse" role="tabpanel" id="oaie">
                <div class="card-block">
                  <p align="center" class="corTexto">DinfoAPP é um projeto da matéria Desenvolvimento De Aplicações WEB que se estende desde o mês de junho até o final do ano, os alunos têm que elaborar um site com todas as suas páginas e fazê-lo funcionar.</p>
                </div>
              </div>
            </div>
          </div>
            </div>
      </div>
    
   
   	<script src="../assets/js/jquery-3.1.0.js"></script>
	<script src="../assets/js/bootstrap.js"></script>  


</body>

</html>